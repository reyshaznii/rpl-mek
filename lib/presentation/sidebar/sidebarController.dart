import 'package:get/get.dart';

class SidebarController extends GetxController {
  var count = 0.obs;
  increment() => count++;
}
